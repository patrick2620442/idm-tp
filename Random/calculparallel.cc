#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <time.h>

#define NOMBRE_TIRAGE 1000000000

#include "CLHEP/Random/MTwistEngine.h"


//  L'objectif de ce programme est la simulation d'un grand nombre (NOMBRE_TIRAGE) de tirages pour obtenir une approximation
//  de PI par la methode de Monte-Carlo. Contrairement à ./calculsequentiel , cette simulation n'est pas répétée 
//  mais nous disposons d'un fichier    bashCalculPi.sh   qui effectuera 10 réplications de ce programme en arrière-plan

//  A titre indicatif, le fichier .sh est de la forme :
//        for ((i = 0; i < 10; i++ )) do
//            time "./calculparallel" "status$i" &
//        done

//  Le temps de calcul est donné sur le terminal pour chaque réplication de  ./calculparallel
//  ainsi que l'estimation de PI


int main (int argc, char** argv)
{
    clock_t tStart = clock();
    CLHEP::MTwistEngine * current_status = new CLHEP::MTwistEngine();

    double random_x;
    double random_y;
    unsigned int inside_circle_nbr = 0;
    double estimated_pi;
     

    current_status->restoreStatus(argv[1]);//("status" + std::to_string(i)).c_str());

    for (int j = 0; j < NOMBRE_TIRAGE; ++j) {
        random_x = current_status->flat();
        random_y = current_status->flat();

        if (random_x * random_x + random_y * random_y <= 1) inside_circle_nbr++;
    }

    estimated_pi = 4 * ((double)inside_circle_nbr / NOMBRE_TIRAGE);
    std::cout << estimated_pi << "   Temps de calcul : " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    delete current_status;

    return 0;
}

