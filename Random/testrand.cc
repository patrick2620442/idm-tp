#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <cstdio>

#include "CLHEP/Random/MTwistEngine.h"

int main ()
{
   CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();

   int fs;
   double f;
   unsigned int nbr;

   s->saveStatus("statut0");

   for(int i = 1; i < 10; i++)
   {
     f = s->flat();

     printf("%f\n", f); // ou mieux cout << f << endl;

   }

   s->restoreStatus("statut0");
   
   for(int i = 1; i < 10; i++)
   {
     f = s->flat();

     printf("%f\n", f); // ou mieux cout << f << endl;

   }

   delete s;

   return 0;
}

