#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <time.h>

#define NOMBRE_TIRAGE 1000000000
#define NOMBRE_REPLICATION 10


#include "CLHEP/Random/MTwistEngine.h"


//  L'objectif de ce programme est la simulation d'un grand nombre (NOMBRE_TIRAGE) de tirages pour obtenir une approximation
//  de PI par la methode de Monte-Carlo. Cette simulation est répétée plusieurs (NOMBRE_REPLICATION) fois.

//  Dans cette simulation séquentielle, nous générons 2 milliards de points * 10 fois, en restorant à chaque réplication
//  un des status générés par le programme    ./creationstatus

//  Les résultats des 10 réplications seront affichés dans le terminal avec leurs temps d'execution par boucle de réplications :
//      Chacun des 10 réplications affichent leur estimations de PI + leur temps d'execution, par exemple :
//          3.1409   Temps de calcul : 0.040788
//          3.14029   Temps de calcul : 0.040827

//  Bien entendu, les prérequis sont la bonne génération des différents fichiers "satusX" de ./creationstatus
//  avec le même nombre de tirage, soit 2 milliards (ou au moins, que le nombre de tirage de creationstatus soit assez grand).
//  Pour etre plus clair :
//    ./creationstatus crée les "statusX" en générant 2 milliards de nombres aléatoires
//    ./calculsequentiel utilise ces "statusX" en faisaint 10 réplications de 1 milliard de tirages, mais a chaque tirage,
//        il génère 2 nombres : ramdom_x et ramdom_y, pour un total de 2 milliards de points, d'où la correspondance

int main ()
{
  clock_t tStart;
  CLHEP::MTwistEngine * current_status = new CLHEP::MTwistEngine();

  double random_x;
  double random_y;
  unsigned int inside_circle_nbr;
  double estimated_pi;
   
  for(int i = 0; i < NOMBRE_REPLICATION; i++)
  {
    inside_circle_nbr = 0;
    tStart = clock();
    
      
    current_status->restoreStatus(("status" + std::to_string(i)).c_str());

    for (int j = 0; j < NOMBRE_TIRAGE; ++j) {
      random_x = current_status->flat();
      random_y = current_status->flat();

      if (random_x * random_x + random_y * random_y <= 1) inside_circle_nbr++;
    }

    estimated_pi = 4 * ((double)inside_circle_nbr / NOMBRE_TIRAGE);

    //affichage resultat + temps de calcul
    std::cout << estimated_pi << "   Temps de calcul : " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;
  }

  delete current_status;

  return 0;
}

