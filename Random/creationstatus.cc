#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>

#include "CLHEP/Random/MTwistEngine.h"

#define NOMBRE_STATUS 10
#define NOMBRE_TIRAGE 2000000000

//  L'objectif de ce programme est la création des status. Leur nombre est déterminé par la macro NOMBRE_STATUS.
//  Le programme tourne, pour notre cas, 2 milliards * 10 itérations.
//  Un succès se traduit par l'apparition de 10 fichiers de "status0" à "status9" et de la ligne sur le terminal :
//      Success in creating 10 statuses

//  REMARQUE : comme ce qui nous interesse est le début de chaque itération, on peut juste sauvegarder
//  la dernière, soit "status9", sans générer les 2 milliards de nombres aléatoires suivants

int main ()
{
   CLHEP::MTwistEngine * current_status = new CLHEP::MTwistEngine();
   double random_generated_number;

   for(int i = 0; i < NOMBRE_STATUS; i++)
   {
        //sauvegarde de "statusi"
        current_status->saveStatus(("status" + std::to_string(i)).c_str());

        //generation des nombres aleatoires pour atteindre le prochain status
        for (int j = 0; j < NOMBRE_TIRAGE; ++j) {
            random_generated_number = current_status->flat();
        }
   }

   delete current_status;

   std::cout << "Success in creating " << NOMBRE_STATUS << " statuses" << std::endl;

   return 0;
}

