#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <time.h>
#include <thread>
#include <mutex>

#define NOMBRE_TIRAGE 2000

#include "CLHEP/Random/MTwistEngine.h"

std::mutex mutex;


double monte_carlo_pi(int nombre_de_tirage, std::string nom_status) {
    CLHEP::MTwistEngine * current_status = new CLHEP::MTwistEngine();
    double random_x;
    double random_y;
    unsigned int inside_circle_nbr = 0;
    double estimated_pi;
        
    current_status->restoreStatus(nom_status.c_str());//("status" + std::to_string(i)).c_str());

    for (int j = 0; j < nombre_de_tirage; ++j) {
        random_x = current_status->flat();
        random_y = current_status->flat();

        if (random_x * random_x + random_y * random_y <= 1) inside_circle_nbr++;
    }

    estimated_pi = 4 * ((double)inside_circle_nbr / nombre_de_tirage);
    delete current_status;

    std::cout << estimated_pi << std::endl;
    return estimated_pi;
} 



template <int N>
void for_parallele() {
    std::thread t[N];
    //double * resultat = new double[N];

    for (int i = 0; i < N; ++i) {
        mutex.lock();
        t[i] = std::thread(monte_carlo_pi(NOMBRE_TIRAGE, ("status" + std::to_string(i)).c_str()));
        //std::cout << "[" << n << "] zzz..." << std::endl;
        mutex.unlock();
    }
} 
//for_parallele<4>(0,taille, [&] (unsigned i) { a[i] = ++compteur; } );




int main (int argc, char** argv)
{
    clock_t tStart = clock();

    //double estimation_de_pi = new double[10];
    
    for_parallele<10>();

    

    
    std::cout << "   Temps de calcul : " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    return 0;
}

